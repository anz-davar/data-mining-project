create database udemy_final;


use udemy_final;

CREATE TABLE `category` (
  `url` varchar(200) NOT NULL,
  `total_course_number` int(11) DEFAULT NULL,
  `questions_by_tag` int(20),
  PRIMARY KEY (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `topics_list` (
  `url` varchar(200) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `general_topic` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `course` (
  `url` VARCHAR(200) NOT NULL,
  `title` VARCHAR(200) NULL,
  `avg_rating` DECIMAL(6,3) NULL,
  `num_ratings` INT NULL,
  `price` DECIMAL(6,3) NULL,
  `length` DECIMAL(6,3) NULL,
  `level` VARCHAR(200) NULL,
  `description` VARCHAR(2000) NULL,
  `instructor` VARCHAR(2000) NULL,
  `lectures` INT NULL,
  PRIMARY KEY (`url`));

CREATE TABLE `course_category` (
  `course_url` varchar(200) NOT NULL,
  `category_url` varchar(200) NOT NULL,
  PRIMARY KEY (`course_url`,`category_url`),
  KEY `fk_category_idx` (`category_url`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  
