from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from random import randint
import time
import logging
from os import environ, path

from configuration import *

logger = logging.getLogger('udemy')


def create_chrome_driver(headless=USE_HEADLESS_BROWSER):
    """" Creates new instance of chrome driver.
    Paramter:
    headless - work in headless mode. Non-headless mode is used only for simple tests."""
    if headless:
        # Lists of user agents and window sizes to take from.
        user_agent = ['Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36']
        window_size = ['1349x609', '1349x626', '1200x600', '1333x609']
        # Setting options for headless browser.
        options = webdriver.ChromeOptions()
        options.add_argument(f'window-size={window_size[randint(0, len(window_size) - 1)]}')
        options.add_argument('headless')
        options.add_argument(f'user-agent={user_agent[randint(0, len(user_agent) - 1)]}')
        driver = webdriver.Chrome(options=options)
        return driver
    return webdriver.Chrome()


class ScrapingFunctions:
    """Class that unites our scraping functions."""
    @staticmethod
    def get_all_topics(soup):
        """Gets all topics urls from Udemy's topics page."""
        topics = soup.select('div.main-content div.container ul li a')
        topics = [t['href'] for t in topics]
        topics = ['https://www.udemy.com' + t if t[0] == '/' else t for t in topics]
        return topics

    @staticmethod
    def course_from_category_page(course):
        """Gets soup object of a single course in category page.
        Returns dict with course data."""
        course_data = {}

        url = course.select('a')[0]['href']
        if url[0] == '/':
            url = 'https://www.udemy.com' + url
        course_data['url'] = url

        title = course.select('div[data-purpose="search-course-card-title"] h4')
        title = bs.get_text(title[0])
        course_data['title'] = title

        price = bs.get_text(course.select('div[data-purpose="course-price-text"] span')[1])
        if price.strip() == 'Free':
            price = 0.0
        else:
            price = float(''.join([d for d in price if d.isdigit() or d == '.']))
        course_data['price'] = price

        general_data = course.select(
            'div[class*="list-view-course-card--meta-with-badge"] span[class*="list-view-course-card--meta-item"]')

        lectures = bs.get_text(general_data[0])
        lectures = int(float(''.join([d for d in lectures if d.isdigit()])))
        course_data['lectures'] = lectures

        length = bs.get_text(general_data[1])
        length = float(''.join([d for d in length if d.isdigit() or d == '.']))
        if bs.get_text(general_data[1])[-4:] == 'mins':
            length /= 60
            length = round(length, 2)
        course_data['length'] = length

        level = bs.get_text(general_data[2])
        course_data['level'] = level

        description_and_instructor = course.select('div[class*="list-view-course-card--headline-and-instructor"] span')
        description = bs.get_text(description_and_instructor[0])
        course_data['description'] = description

        instructor = bs.get_text(description_and_instructor[1])[3:]
        course_data['instructor'] = instructor

        avg_rating = course.select('div[class*="list-view-course-card--rating"] span:nth-of-type(1)')
        avg_rating = float(bs.get_text(avg_rating[0]))
        course_data['avg_rating'] = avg_rating

        num_ratings = course.select('div[class*="list-view-course-card--rating"] span')
        num_ratings = bs.get_text(num_ratings[-1])
        num_ratings = int(float(''.join([d for d in num_ratings if d.isdigit()])))
        course_data['num_ratings'] = num_ratings
        return course_data

    @staticmethod
    def scrape_category(category_url, page_limit=1, driver=None):
        """ Returns course data from category page.

        Parameters:

            category_url - category page to scrape.

            page_limit - number of course pages to take from the category (each one contains 12 courses).

            driver - ability to pass a driver to use (not recommended, Udemy will block after the first page).

        Returns: dict with category data (another dict) and courses data (list of dicts).
        """

        # get page
        if not driver:
            driver = create_chrome_driver(headless=USE_HEADLESS_BROWSER)
        driver.get(category_url)
        time.sleep(WAIT_FOR_PAGE_LOAD)
        soup = bs(driver.page_source, 'html.parser')

        # set data lists
        urls = []
        courses_data_dicts = []

        # iterate over course pages in category page and get courses data
        page_counter = 0
        num_courses = ScrapingFunctions.find_num_courses_in_category(soup)
        last_page = int((num_courses - num_courses % COURSES_PER_PAGE) / COURSES_PER_PAGE + 1)
        while page_counter < page_limit and page_counter < last_page:
            try:
                urls.extend(ScrapingFunctions.course_urls_from_category(soup))
                courses = soup.select('div[data-purpose="search-course-cards"]')
                for course in courses:
                    course_data = ScrapingFunctions.course_from_category_page(course)
                    courses_data_dicts.append(course_data)
            except Exception as e:
                logger.exception(f'Exception while scraping from category {category_url}')
                print('Error at ' + category_url + '\ndetails: ' + str(e))

            page_counter += 1
            time.sleep(randint(MIN_WAIT_PAGINATOR, MAX_WAIT_PAGINATOR))
            if page_counter < page_limit and page_counter < last_page:
                ScrapingFunctions.click_next(driver)
                time.sleep(WAIT_FOR_PAGE_LOAD)
                soup = bs(driver.page_source, 'html.parser')

        driver.close()
        category_data = {'category': category_url, 'category_urls': urls, 'number_of_courses': num_courses}
        return {'category_data': category_data, 'courses_data_dicts': courses_data_dicts}

    @staticmethod
    def scrape_topics(driver=None):
        """Returns all topics data from topics page."""

        # get page
        url = 'https://www.udemy.com/topics/'
        if not driver:
            driver = create_chrome_driver(headless=USE_HEADLESS_BROWSER)
        driver.get(url)
        time.sleep(WAIT_FOR_PAGE_LOAD)
        soup = bs(driver.page_source, 'html.parser')

        # get topics div and general categories names
        main_div = soup.select('div[class="container pb20"]')[0]
        general_topics = main_div.select('h3')
        general_topics = [bs.get_text(t) for t in general_topics]

        # get the topics themselves
        all_sub_topics = main_div.select('ul')
        topics_list = []
        for i, ul in enumerate(all_sub_topics):
            sub_topics = ul.select('li a')
            for st in sub_topics:
                topic_dict = {}
                url = st['href']
                if url[0] == '/':
                    url = 'https://www.udemy.com' + url
                topic_dict['url'] = url
                topic_dict['name'] = bs.get_text(st)
                topic_dict['general_topic'] = general_topics[i]
                topics_list.append(topic_dict)
        driver.close()
        return topics_list

    @staticmethod
    def course_urls_from_category(soup):
        """ Returns the links to the courses listed in the category page."""
        urls = soup.select('div[class^="list-view-course-card--course-card-wrappe"]')
        urls = [url.find('a', href=True)['href'] for url in urls]
        urls = ['https://www.udemy.com' + u if u[0] == '/' else u for u in urls]
        return urls

    @staticmethod
    def click_next(driver):
        """ Clicks 'next' button on course list (JavaScript pagination) in a category page"""
        try:
            python_button = driver.find_element_by_css_selector('ul.pagination li:last-child a')  # find "next" button
        except NoSuchElementException:
            python_button = driver.find_element_by_css_selector('ul.pager li:last-child a')  # another configuration
        python_button.click()  # click button

    @staticmethod
    def find_num_courses_in_category(soup):
        """ Returns the number of the courses within a category."""
        last = soup.select('h2[class^="filtered-paginated-course-list--course-count"]')
        last = bs.get_text(last[0])
        if last == 'Full course catalog':
            num = soup.select('div[class^="course-filters--course-count-desktop"]')
            num = bs.get_text(num[0])
            num = int(float(''.join([d for d in num if d.isdigit()])))
            return num
        last = int(float(''.join([d for d in last if d.isdigit()])))
        return last

    @staticmethod
    def import_test():
        """ Used to test to importation of the functions, as a quick smoke test"""
        return True
