import requests
from bs4 import BeautifulSoup as bs4
import time
from selenium import webdriver


def get_title(soup):
    return soup.title.get_text()

def get_number_of_ratings(soup):
    list(soup.find(id="schema_markup").children)
    rating = soup.select('span[id^="rate-count-value"]')
    return rating[0].next_sibling.strip()

def created_by(soup):
    created_by  = soup.select('a.instructor-links__link')
    return created_by[0].get_text().strip("\n ")

def get_price(soup):
    price = soup.select('div[class*="price-text--base-price__container"]')  # returns a list that might be > 1
    price[0].contents[0].get_text()

def get_total_duration(soup):
    total_duration = soup.select('span.curriculum-header-length')[0].get_text()
    return total_duration.strip('\n ')

def get_description2(soup):
    enro = soup.select('div[data-purpose="collapse-description-btn"]')
    return enro[0].get_text()

def get_categiries_and_subcategories(soup):
    categories_list = []
    for category in soup.select('a[class*="topic-menu--topic-menu__link--"]'):
        categories_list.append(category.get_text())
    return categories_list


def get_students_feedback_distribution(soup):
    feedback_list = []
    for EachPart in soup.select('span[data-purpose="percent-label"]'):
        feedback_list.append(EachPart.get_text())
    return feedback_list

def import_test():
    print('something')
"""
url = "https://www.udemy.com/java-in-depth-become-a-complete-java-engineer/"
driv = webdriver.Chrome()
driv.get('https://www.udemy.com/java-in-depth-become-a-complete-java-engineer/')
time.sleep(10)
soup=bs4(driv.page_source)
#page = requests.get(url,headers = {'User-Agent':'Mozilla/5.0'})
#soup = bs4(page.content, "html.parser")
print(get_title(soup))
print(get_number_of_ratings(soup))
print(created_by(soup))
print(get_price(soup))
print(get_total_duration(soup))
print(get_description2(soup))
print(get_categiries_and_subcategories(soup))
print(get_students_feedback_distribution(soup))


"""