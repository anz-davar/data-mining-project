import mysql.connector
import random
from configuration import *
import platform
import logging

logger = logging.getLogger('udemy')


class OurSql:
    """Class providing context manager for mySQL, for later use."""
    def __init__(self, user=AWS_USER, password=AWS_PASS, db=DB_NAME):
        if platform.platform()[:7] == 'Windows':  # use local settings on local tests.
            self.user = USERNAME
            self.db = db
        else:
            self.user = user
            self.password = password
            self.db = db

    def __enter__(self):
        """Create DB connection and cursor objects"""
        if platform.platform()[:7] == 'Windows':
            self.cnx = mysql.connector.connect(user=self.user,
                                               database=self.db)
        else:
            self.cnx = mysql.connector.connect(user=self.user,
                                           password=self.password,
                                           database=self.db)
        self.cursor = self.cnx.cursor()
        return self.cnx, self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Close cursor and connection"""
        self.cursor.close()
        self.cnx.close()


class SqlActions:
    """Class that holds the operation we need to do with mySQL.
    We use the OurSql context manager to ensure the DB connection will be closed in the end no matter what."""
    def __init__(self, our_sql_instance):
        """Create instance of OurSql context manager to use."""
        if type(our_sql_instance) != OurSql:
            raise TypeError('our_sql_instance should be OurSql object')
        self.context_manager = our_sql_instance

    def execute(self, query):
        """Execution if given query."""
        with self.context_manager as cm:
            cnx, cursor = cm[0], cm[1]
            cursor.execute(query)
            cnx.commit()

    def executemany(self, query, data):
        """Execution if given query for data, using executemany."""
        with self.context_manager as cm:
            cnx, cursor = cm[0], cm[1]
            cursor.executemany(query, data)
            cnx.commit()

    def read(self, query):
        """Execution of select query that returns the results."""
        with self.context_manager as cm:
            cnx, cursor = cm[0], cm[1]
            cursor.execute(query)
            results = cursor.fetchall()
            return results

    def prepare_dict_to_insert(self, data_dict):
        """Preparing data_dict for insert into mySQL by changing values to permitted data types."""
        for key, value in data_dict.items():
            if type(value) in [list, tuple]:
                value = str(value)
                value = value.replace("'", "")
                data_dict[key] = value
        return data_dict

    def insert_dict(self, table_name, data_dict, insert_ignore=True):
        """Used to prepare the right insert query for a given data dictionary and table."""
        if insert_ignore:
            ignore_part = 'ignore'
        else:
            ignore_part = ''

        data_dict = self.prepare_dict_to_insert(data_dict)

        with self.context_manager as cm:
            cnx, cursor = cm[0], cm[1]
            placeholders = ", ".join(["%s"] * len(data_dict))
            query = "insert {ignore} into `{table}` ({columns}) values ({values});"\
                .format(ignore=ignore_part,
                        table=table_name,
                        columns=",".join(data_dict.keys()),
                        values=placeholders)
            try:
                cursor.execute(query, list(data_dict.values()))
                cnx.commit()
            except Exception:
                logger.exception(f'Error in executing query {query}')
                raise


class TestSqlCClasses:
    """Test class for testing SQL classes and methods."""
    def __init__(self, our_sql_instance):
        """CReates instance of context manager."""
        self.oursql = our_sql_instance
        self.sqlactions = SqlActions(self.oursql)

    def check_functions(self):
        """Tests all SQL operations."""
        try:
            created_tables = False
            query = 'create table codetests1 (rand bigint)'
            self.sqlactions.execute(query)  # test execute
            query = 'create table codetests2 (rand bigint, txt varchar(20))'
            self.sqlactions.execute(query)
            query = 'create table codetests3 (rand bigint, txt varchar(20))'
            self.sqlactions.execute(query)
            created_tables = True

            some_number = random.randint(1, 9999999999)
            query = 'insert into codetests1 values (%d)' % some_number
            self.sqlactions.execute(query)  # test insert
            data = self.sqlactions.read('select * from codetests1')  # test read
            assert data[0][0] == some_number

            first_number = random.randint(1, 9999999999)
            second_number = random.randint(1, 9999999999)
            first_string = str(random.randint(1, 9999999999))
            second_string = str(random.randint(1, 9999999999))
            query = 'insert into codetests2 values (%s, %s)'
            rows = [(first_number, first_string), (second_number, second_string)]
            self.sqlactions.executemany(query, rows)  # test insert of many rows
            data = self.sqlactions.read('select * from codetests2')
            assert data == rows

            another_number = random.randint(1, 9999999999)
            another_string = str(random.randint(1, 9999999999))
            test_dict = {'txt': another_string, 'rand': another_number}
            self.sqlactions.insert_dict('codetests3', test_dict)  # test insert_dict
            data = self.sqlactions.read('select * from codetests3')
            assert data == [(another_number, another_string)]

        except Exception:
            logger.exception(f'Failed during tests of sqlActions. Current query: {query}')
            raise

        finally:
            if created_tables:  # If tests table were created, drop them.
                query = 'drop table codetests1'
                self.sqlactions.execute(query)
                query = 'drop table codetests2'
                self.sqlactions.execute(query)
                query = 'drop table codetests3'
                self.sqlactions.execute(query)

        return True  # If all good


def save_course_data(course_data):
    """"saves course data to db"""
    mysql_manager = OurSql()
    insert_manager = SqlActions(mysql_manager)
    insert_manager.insert_dict('course', course_data)


def save_topic_data(topic_data):
    """"saves topic data to db (topic data from topics list)"""
    mysql_manager = OurSql()
    insert_manager = SqlActions(mysql_manager)
    insert_manager.insert_dict('topics_list', topic_data)


def save_category_data(scraped_category_data):
    """"saves category data to db (category data from category page)"""

    # organize data by type, as scraped_category_data contains both the category data and the courses data.
    category = scraped_category_data['category_data']
    courses = scraped_category_data['courses_data_dicts']

    # arrange data for category-course table
    cat_data = {'url': category['category'],
                'total_course_number': category['number_of_courses']}
    cat_course_data = []
    for course_url in category['category_urls']:
        cat_course_dict = {'course_url': course_url,
                           'category_url': category['category']}
        cat_course_data.append(cat_course_dict)

    # initiate context manager for mysql and insert data
    mysql_manager = OurSql()
    insert_manager = SqlActions(mysql_manager)
    insert_manager.insert_dict('category', cat_data)
    for item in cat_course_data:
        insert_manager.insert_dict('course_category', item)
    for course in courses:
        insert_manager.insert_dict('course', course)


if __name__ == '__main__':
    test_oursql = OurSql()
    test_actions = TestSqlCClasses(test_oursql)
    assert test_actions.check_functions()
