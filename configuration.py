# Configuration file

# sql
DB_NAME = 'udemy_final'
USERNAME = 'itc'
AWS_USER = 'root'
AWS_PASS = 'itc'

# scraping functions
USE_HEADLESS_BROWSER = True

# main
COURSE_LIMIT = 999999
WAIT_FOR_PAGE_LOAD = 20
# TODO: https://stackoverflow.com/questions/26566799/wait-until-page-is-loaded-with-selenium-webdriver-for-python
MIN_SLEEP = 51
MAX_SLEEP = 75
SCHEDULE_JOB = False
JOB_INTERVAL = 60 * 8  # start again every 8 hours
COURSES_PER_PAGE = 12
MIN_WAIT_PAGINATOR = 5
MAX_WAIT_PAGINATOR = 15

# API
API_URL_START = 'https://api.stackexchange.com/2.2/tags/'
API_URL_END = '/info?order=desc&sort=popular&site=stackoverflow'