from scraping_functions import ScrapingFunctions
from SQL_functions import *
from configuration import *
from datetime import timedelta
from random import randint
import time
import sys
from datetime import datetime
import click
import requests
import platform
import logging
from os import environ, path
if platform.platform()[:7] != 'Windows':
    from apscheduler.schedulers.blocking import BlockingScheduler

assert ScrapingFunctions.import_test()

log_file = environ.get('UDEMY_LOG') or path.expanduser('~/.udemy.log')
logging.basicConfig(
    filename=log_file,
    level=logging.INFO,
    format='%(asctime)s:%(name)s:%(levelname)s: %(message)s',
)

logger = logging.getLogger('udemy')


def pause_during_iteration(index, sleep_first=True):
    """Used to determine the wait times between requests to the website.

    Parameters:

    index - index of iteration.

    sleep_first - When True, sleep time is mandatory even for the first iteration.

    Output: None (the function will sleep for the determined time)."""
    if sleep_first and index == 0:
        time.sleep(randint(MIN_SLEEP, MAX_SLEEP))
    elif index > 0:
        time.sleep(randint(MIN_SLEEP, MAX_SLEEP))
    if index > 0 and index % 3 == 0:
        time.sleep(randint(MIN_SLEEP, MAX_SLEEP))
    if index > 0 and index % 5 == 0:
        time.sleep(randint(MIN_SLEEP, MAX_SLEEP))
    if randint(1, 7) == 1:
        time.sleep(randint(MIN_SLEEP, MAX_SLEEP))


def get_num_questions_by_tag(tag):
    """"gets number of questions by tag from api.

    Input: the tag to search.

    Output: Number of questions if tag was found, -1 if not."""

    url = API_URL_START + tag + API_URL_END
    resp = requests.get(url=url)
    data = resp.json()
    if data['items']:
        return data['items'][0]['count']
    else:
        return "-1"


def enrich_data(enrich_all=True, url=''):
    """"reads data from database and inserts api data.

    Parameters:

    enrich_all - When True updates all rows in DB.

    url - If enrich_all is False, gets the number of questions just for a specific category.
    (url serves as a primary key in the DB)"""
    mysql_manager = OurSql()
    insert_manager = SqlActions(mysql_manager)

    if enrich_all:  # when scrape type is everything and topics we update the whole table
        categories = insert_manager.read("select * from category;")
        for category in categories:
            questions_count = get_num_questions_by_tag(category[0].rstrip('/').split('/')[-1])
            #print("update  category set questions_by_tag = " + f'{questions_count}' + " where url ='" + category[0] + "' ")
            #insert_manager.execute("update  category set questions_by_tag = " + f'{questions_count}' + " where url ='" + category[0] + "' ")
            sql_query = "update category set questions_by_tag = {} where url ='{}'".format(questions_count, category[0])
            insert_manager.execute(sql_query)
            logger.info(sql_query)

    else:  # when scrape type is category or urls update onle specific category
        categories = insert_manager.read("select * from category where url = '{}'".format(url))
        for category in categories:
            questions_count = get_num_questions_by_tag(category[0].rstrip('/').split('/')[-1])
            sql_query = f"update category set questions_by_tag = {questions_count} where url = '{category[0]}'"
            insert_manager.execute(sql_query)
            logger.info(sql_query)



@click.command()
@click.argument('scrape_type', default='everything')
@click.argument('num_pages', default=1)
@click.argument('url', default='')
def main(scrape_type, num_pages, url):
    """"Scrapes udemy.com according to defined parameters, enriches data with StackExchange API and saves it in the DB.

    Parameters:

    scrape_type - choose what to scrape.
        "everything": (default), the crawler will start
    by reading the full topics list and then will scrape course data from each category.
    (Note: a topic (e.g. Python) is a sub-category. Categories (e.g. Development) pages are similar to topics pages,
    so practically we can say every topic is a category. But not every category is a topic, because we can't find
    them on the full topics list and because they just contain the courses from their sub-categories.)
        "continued": the crawler will check which topics are not in the scraped categories and will scrape them.
        "topics": the crawler will get just the topics list.
        "category": the crawler will scrape a single category.

    num_pages - number of course pages to scrape from each category. Each category page holds 12 courses.

    url - url of category when scrape_type is category.
    """

    scrape_type = scrape_type.lower().strip()
    if scrape_type not in ['category', 'continued', 'topics', 'everything']:
        print("Please choose type from 'category', 'continued', 'topics', 'everything'")
        sys.exit(1)
    else:
        print(f'Scraping {scrape_type}...')
        logger.info(f'Scraping {scrape_type}')

    if scrape_type == 'everything':
        topics = ScrapingFunctions.scrape_topics()
        for topic in topics:
            save_topic_data(topic)  # first save all topics, then crawl category pages.
        for i, topic in enumerate(topics):
            if topic['url'].lower().find('selenium') > -1:  # skip "honey pot" (false topic named selenium-training...)
                continue
            pause_during_iteration(i)
            topic_url = topic['url']
            logger.info(f'Scraping {i+1} topic from {topic_url}')
            category = ScrapingFunctions.scrape_category(topic_url)
            save_category_data(category)
            enrich_data(enrich_all=False, url=topic_url)

    if scrape_type == 'topics':
        topics = ScrapingFunctions.scrape_topics()
        for topic in topics:
            save_topic_data(topic)

    if scrape_type == 'continued':
        # read topics list from DB, scrape those not in scraped categories table.
        # (Using separate tables for topics list and scraped categories gives us the comfort of using INSERT IGNORE)
        # when saving category data.)
        query = """
        SELECT url
        FROM topics_list
        WHERE url NOT IN
        (SELECT url
        FROM category)
        """
        db_connection_handler = OurSql()
        sql_handler = SqlActions(db_connection_handler)
        unscraped_topics = sql_handler.read(query)
        unscraped_topics = [x[0] for x in unscraped_topics]
        logger.info('Continued scraping for {} topics.'.format(len(unscraped_topics)))
        for i, topic in enumerate(unscraped_topics):
            if topic.lower().find('selenium') > -1:  # skip over honey pot
                continue
            pause_during_iteration(i, sleep_first=False)
            logger.info(f'Scraping {i+1} topic from {topic}')
            category = ScrapingFunctions.scrape_category(topic)
            save_category_data(category)
            enrich_data(enrich_all=False, url=topic)

    if scrape_type == 'category':
        cat_results = ScrapingFunctions.scrape_category(
            category_url=url,
            page_limit=num_pages)
        save_category_data(cat_results)
        enrich_data(enrich_all=False, url=url)


if __name__ == '__main__':
    """Welcome to udemy.com scraper from Shay and Anzor!!!!
    We have a very friendly and easy to use cli tool for you.
    ALl you need is to enter just 3 parameters:
    1) scrape_type: choose from 'everything' (default), 'continued', 'category', 'topics'.
    2) num_pages : number of pages to scrape from each category (default is 1).
    3) url: url of the category to be scraped on category mode.
    Please note the crawler takes long breaks between pages to avoid being blocked by Udemy."""
    try:
        if platform.platform()[:7] == 'Windows':  # True for local tests
            main()
        elif SCHEDULE_JOB:
            scheduler = BlockingScheduler()
            scheduler.add_job(main, 'interval', start_date=datetime.now() + timedelta(seconds=3), minutes=JOB_INTERVAL)
            scheduler.start()
        else:
            main()
    except Exception:
        logger.exception('Error in main')
        raise
